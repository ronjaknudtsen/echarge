// Create global variables to hold coordinates and the map.
var _my_module_user_latitude = null;
var _my_module_user_longitude = null;
var _my_module_map = null;

/**
 * Implements hook_menu().
 */
function my_module_menu() {
  var items = {};
  items['hello_world'] = {
    title: 'DrupalGap',
    page_callback: 'my_module_hello_world_page'
 //   pageshow: 'runWebsocketServer';
  };
  items['map'] = {
    title: 'Map',
    page_callback: 'charging_stations_page',
    //page_callback: 'my_module_map',
    //pageshow: 'custom_pageshow'
  };
  return items;
}





function my_module_node_page_view_alter_ladestasjoner(node, options){
  try {
    var content = {};
    
    content['my_markup'] = {
      markup: '<div id="testme"></div>'
    };
    options.success(content);
    runWebsocketServer();
  }
  catch(error){ console.log('my_module_node_page_view_alter_chargingstations' + error); }
}

 function runWebsocketServer(){
  console.log("RUN WEBSOCKET SERVER");
  if ("WebSocket" in window){
     alert("WebSocket is supported by your Browser!");
     
     // Let us open a web socket
     var ws = new WebSocket("ws://realtime.nobil.no/api/v1/stream?apikey=a1060d7d9c5a88b1a55e1a1a7cd6dacb");


     ws.onmessage = function (evt){ 
        var received_msg = JSON.parse(evt.data);
        console.log("Message is received..."+received_msg);

     };

     /*ws.onclose = function()
     { 
        // websocket is closed.
        alert("Connection is closed..."); 
     };*/
  }

  else
  {
     // The browser doesn't support WebSocket
     alert("WebSocket NOT supported by your Browser!");
  }  
}
/**
 * The callback for the "Hello World" page.
 */
function my_module_hello_world_page() {
  var content = {};
  content['my_button'] = {
    theme: 'button',
    text: 'Hello World',
    attributes: {
      onclick: "drupalgap_alert('Hi!')"
    }
  };
  return content;
}

/**
 * The map page callback.
 */
function my_module_map() {
  try {
    var content = {};
    var map_attributes = {
      id: 'my_module_map',
      style: 'width: 100%; height: 320px;'
    };
    content['map'] = {
      markup: '<div ' + drupalgap_attributes(map_attributes) + '></div>'
    };
    return content;
  }
  catch (error) { console.log('my_module_map - ' + error); }
}

function charging_stations_page(){

}

/**
 * The map pageshow callback.
 *//*
function my_module_map_pageshow() {
  try {
    navigator.geolocation.getCurrentPosition(
      
      // Success.
      function(position) {

        // Set aside the user's position.
        _my_module_user_latitude = position.coords.latitude;
        _my_module_user_longitude = position.coords.longitude;
        
        // Build the lat lng object from the user's position.
        var myLatlng = new google.maps.LatLng(
          _my_module_user_latitude,
          _my_module_user_longitude
        );
        
        // Set the map's options.
        var mapOptions = {
          center: myLatlng,
          zoom: 11,
          mapTypeControl: true,
          mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
          },
          zoomControl: true,
          zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
          }
        };
        
        // Initialize the map, and set a timeout to resize properly.
        _my_module_map = new google.maps.Map(
          document.getElementById("my_module_map"),
          mapOptions
        );
        setTimeout(function() {
            google.maps.event.trigger(_my_module_map, 'resize');
            _my_module_map.setCenter(myLatlng);
        }, 500);
        
        // Add a marker for the user's current position.
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: _my_module_map,
            icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
        });
        
      },
      
      // Error
      function(error) {
        
        // Provide debug information to developer and user.
        console.log(error);
        drupalgap_alert(error.message);
        
        // Process error code.
        switch (error.code) {

          // PERMISSION_DENIED
          case 1:
            break;

          // POSITION_UNAVAILABLE
          case 2:
            break;

          // TIMEOUT
          case 3:
            break;

        }

      },
      
      // Options
      { enableHighAccuracy: true }
      
    );
  }
  catch (error) {
    console.log('my_module_map_pageshow - ' + error);
  }
}*/

